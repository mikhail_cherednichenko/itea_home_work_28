const rest = JSON.parse(localStorage.restorationBD);

if(!Array.isArray(rest)){
    throw Error ("...")
}

const restEl = rest.map(({productName, id, productimageUrl, description, keywords, productWeigth, price }) => {
    return `
    <div class="rest">
    <h3 class="rest-name">${productName}</h3>
    <img id="${id}" src="${productimageUrl}"></img>
    <p class="rest-description">${description}</p>
    <p class="rest-price">Price: ${price} UAH</p>
    <p class="rest-weigth">Weigth: ${productWeigth} g</p>
    <div>
    ${keywords.map((el) => {
        return `<span class="badge bg-secondary">${el}</span>`
    }).join("")}
     </div>  
    </div>
    `
})

document.querySelector(".rest-box")
.insertAdjacentHTML("beforeend", restEl.join(""));

