const store = JSON.parse(localStorage.store);

if(!Array.isArray(store)){
    throw Error ("...")
}

const storeEl = store.map(({productName, id, productImage, productDescription, keywords, porductPrice }) => {
    return `
    <div class="store">
    <h3 class="store-name">${productName}</h3>
    <img id="${id}" src="${productImage}"></img>
    <p class="store-description">${productDescription}</p>
    <p class="store-price">Price: ${porductPrice} UAH</p>
    <div>
    ${keywords.map((el) => {
        return `<span class="badge bg-secondary">${el}</span>`
    }).join("")}
     </div>  
    </div>
    `
})

document.querySelector(".store-box")
.insertAdjacentHTML("beforeend", storeEl.join(""));

